﻿namespace Cycle
{
    partial class Data_Load
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelSummary = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.labelAverageAltitude = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelMaxAltitude = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelAveragePower = new System.Windows.Forms.Label();
            this.labelMaxPower = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.totaldistance = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.averagespeed = new System.Windows.Forms.Label();
            this.maxspeed = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMaxHeartRate = new System.Windows.Forms.Label();
            this.labelMinHeartRate = new System.Windows.Forms.Label();
            this.labelAverageHeartRate = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblWeightData = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblVO2MaxData = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStartTimeData = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblSmodeData = new System.Windows.Forms.Label();
            this.lblMaxHR = new System.Windows.Forms.Label();
            this.lblMonitorData = new System.Windows.Forms.Label();
            this.lblRestHR = new System.Windows.Forms.Label();
            this.lblRestHRData = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.lblMaxHRData = new System.Windows.Forms.Label();
            this.lblSmode = new System.Windows.Forms.Label();
            this.lblLengthDAta = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblDateData = new System.Windows.Forms.Label();
            this.lblVOzMax = new System.Windows.Forms.Label();
            this.lblVersionData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.SeaGreen;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(2, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(639, 352);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 132);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(640, 359);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightBlue;
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.labelSummary);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(640, 132);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(254, 359);
            this.panel2.TabIndex = 1;
            // 
            // labelSummary
            // 
            this.labelSummary.AutoSize = true;
            this.labelSummary.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSummary.Location = new System.Drawing.Point(6, 13);
            this.labelSummary.Name = "labelSummary";
            this.labelSummary.Size = new System.Drawing.Size(125, 18);
            this.labelSummary.TabIndex = 40;
            this.labelSummary.Text = "Summary Data";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(146, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(51, 23);
            this.button2.TabIndex = 41;
            this.button2.Text = "Miles";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(200, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(51, 23);
            this.button3.TabIndex = 42;
            this.button3.Text = "km/hr";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel7.BackgroundImage = global::Cycle.Properties.Resources.altitude;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel7.Controls.Add(this.labelAverageAltitude);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.labelMaxAltitude);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Location = new System.Drawing.Point(9, 292);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 55);
            this.panel7.TabIndex = 4;
            // 
            // labelAverageAltitude
            // 
            this.labelAverageAltitude.AutoSize = true;
            this.labelAverageAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAverageAltitude.Location = new System.Drawing.Point(146, 34);
            this.labelAverageAltitude.Name = "labelAverageAltitude";
            this.labelAverageAltitude.Size = new System.Drawing.Size(11, 13);
            this.labelAverageAltitude.TabIndex = 53;
            this.labelAverageAltitude.Text = ".";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Max Altitude";
            // 
            // labelMaxAltitude
            // 
            this.labelMaxAltitude.AutoSize = true;
            this.labelMaxAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxAltitude.Location = new System.Drawing.Point(146, 14);
            this.labelMaxAltitude.Name = "labelMaxAltitude";
            this.labelMaxAltitude.Size = new System.Drawing.Size(11, 13);
            this.labelMaxAltitude.TabIndex = 52;
            this.labelMaxAltitude.Text = ".";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 46;
            this.label9.Text = "Average Altitude";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Crimson;
            this.panel6.BackgroundImage = global::Cycle.Properties.Resources.power;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel6.Controls.Add(this.labelAveragePower);
            this.panel6.Controls.Add(this.labelMaxPower);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(9, 216);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 59);
            this.panel6.TabIndex = 3;
            // 
            // labelAveragePower
            // 
            this.labelAveragePower.AutoSize = true;
            this.labelAveragePower.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAveragePower.Location = new System.Drawing.Point(146, 32);
            this.labelAveragePower.Name = "labelAveragePower";
            this.labelAveragePower.Size = new System.Drawing.Size(11, 13);
            this.labelAveragePower.TabIndex = 55;
            this.labelAveragePower.Text = ".";
            // 
            // labelMaxPower
            // 
            this.labelMaxPower.AutoSize = true;
            this.labelMaxPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxPower.Location = new System.Drawing.Point(146, 11);
            this.labelMaxPower.Name = "labelMaxPower";
            this.labelMaxPower.Size = new System.Drawing.Size(11, 13);
            this.labelMaxPower.TabIndex = 54;
            this.labelMaxPower.Text = ".";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 53;
            this.label7.Text = "Average power";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 52;
            this.label6.Text = "Max Power";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.BackgroundImage = global::Cycle.Properties.Resources.speed;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.totaldistance);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.averagespeed);
            this.panel5.Controls.Add(this.maxspeed);
            this.panel5.Location = new System.Drawing.Point(9, 125);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(217, 85);
            this.panel5.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Total Distance Covered";
            // 
            // totaldistance
            // 
            this.totaldistance.AutoSize = true;
            this.totaldistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totaldistance.Location = new System.Drawing.Point(148, 52);
            this.totaldistance.Name = "totaldistance";
            this.totaldistance.Size = new System.Drawing.Size(11, 13);
            this.totaldistance.TabIndex = 51;
            this.totaldistance.Text = ".";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Max Speed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Average Speed";
            // 
            // averagespeed
            // 
            this.averagespeed.AutoSize = true;
            this.averagespeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averagespeed.Location = new System.Drawing.Point(146, 29);
            this.averagespeed.Name = "averagespeed";
            this.averagespeed.Size = new System.Drawing.Size(11, 13);
            this.averagespeed.TabIndex = 48;
            this.averagespeed.Text = ".";
            // 
            // maxspeed
            // 
            this.maxspeed.AutoSize = true;
            this.maxspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxspeed.Location = new System.Drawing.Point(146, 10);
            this.maxspeed.Name = "maxspeed";
            this.maxspeed.Size = new System.Drawing.Size(11, 13);
            this.maxspeed.TabIndex = 49;
            this.maxspeed.Text = ".";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BackgroundImage = global::Cycle.Properties.Resources.heat_rate;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.labelMaxHeartRate);
            this.panel4.Controls.Add(this.labelMinHeartRate);
            this.panel4.Controls.Add(this.labelAverageHeartRate);
            this.panel4.Location = new System.Drawing.Point(9, 43);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 76);
            this.panel4.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Max Heart Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Min Heart Rate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Average Heart Rate";
            // 
            // labelMaxHeartRate
            // 
            this.labelMaxHeartRate.AutoSize = true;
            this.labelMaxHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxHeartRate.Location = new System.Drawing.Point(146, 14);
            this.labelMaxHeartRate.Name = "labelMaxHeartRate";
            this.labelMaxHeartRate.Size = new System.Drawing.Size(11, 13);
            this.labelMaxHeartRate.TabIndex = 31;
            this.labelMaxHeartRate.Text = ".";
            // 
            // labelMinHeartRate
            // 
            this.labelMinHeartRate.AutoSize = true;
            this.labelMinHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinHeartRate.Location = new System.Drawing.Point(146, 35);
            this.labelMinHeartRate.Name = "labelMinHeartRate";
            this.labelMinHeartRate.Size = new System.Drawing.Size(11, 13);
            this.labelMinHeartRate.TabIndex = 32;
            this.labelMinHeartRate.Text = ".";
            // 
            // labelAverageHeartRate
            // 
            this.labelAverageHeartRate.AutoSize = true;
            this.labelAverageHeartRate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAverageHeartRate.Location = new System.Drawing.Point(146, 52);
            this.labelAverageHeartRate.Name = "labelAverageHeartRate";
            this.labelAverageHeartRate.Size = new System.Drawing.Size(12, 18);
            this.labelAverageHeartRate.TabIndex = 47;
            this.labelAverageHeartRate.Text = ".";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel1.BackgroundImage = global::Cycle.Properties.Resources._09_512;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.lblWeightData);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.lblVO2MaxData);
            this.panel1.Controls.Add(this.lblVersion);
            this.panel1.Controls.Add(this.lblWeight);
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.lblStartTimeData);
            this.panel1.Controls.Add(this.lblLength);
            this.panel1.Controls.Add(this.lblSmodeData);
            this.panel1.Controls.Add(this.lblMaxHR);
            this.panel1.Controls.Add(this.lblMonitorData);
            this.panel1.Controls.Add(this.lblRestHR);
            this.panel1.Controls.Add(this.lblRestHRData);
            this.panel1.Controls.Add(this.lblMonitor);
            this.panel1.Controls.Add(this.lblMaxHRData);
            this.panel1.Controls.Add(this.lblSmode);
            this.panel1.Controls.Add(this.lblLengthDAta);
            this.panel1.Controls.Add(this.lblStartTime);
            this.panel1.Controls.Add(this.lblDateData);
            this.panel1.Controls.Add(this.lblVOzMax);
            this.panel1.Controls.Add(this.lblVersionData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(894, 132);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lblWeightData
            // 
            this.lblWeightData.AutoSize = true;
            this.lblWeightData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblWeightData.Location = new System.Drawing.Point(391, 105);
            this.lblWeightData.Name = "lblWeightData";
            this.lblWeightData.Size = new System.Drawing.Size(47, 14);
            this.lblWeightData.TabIndex = 40;
            this.lblWeightData.Text = "Weight";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(3, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 46);
            this.button1.TabIndex = 3;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblVO2MaxData
            // 
            this.lblVO2MaxData.AutoSize = true;
            this.lblVO2MaxData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVO2MaxData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblVO2MaxData.Location = new System.Drawing.Point(330, 105);
            this.lblVO2MaxData.Name = "lblVO2MaxData";
            this.lblVO2MaxData.Size = new System.Drawing.Size(55, 14);
            this.lblVO2MaxData.TabIndex = 18;
            this.lblVO2MaxData.Text = "VO2Max";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblVersion.Location = new System.Drawing.Point(114, 18);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(51, 14);
            this.lblVersion.TabIndex = 0;
            this.lblVersion.Text = "Version";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblWeight.Location = new System.Drawing.Point(391, 76);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(47, 14);
            this.lblWeight.TabIndex = 10;
            this.lblWeight.Text = "Weight";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblDate.Location = new System.Drawing.Point(330, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(34, 14);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Date";
            // 
            // lblStartTimeData
            // 
            this.lblStartTimeData.AutoSize = true;
            this.lblStartTimeData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartTimeData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblStartTimeData.Location = new System.Drawing.Point(391, 47);
            this.lblStartTimeData.Name = "lblStartTimeData";
            this.lblStartTimeData.Size = new System.Drawing.Size(67, 14);
            this.lblStartTimeData.TabIndex = 17;
            this.lblStartTimeData.Text = "Start Time";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLength.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblLength.Location = new System.Drawing.Point(112, 76);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(47, 14);
            this.lblLength.TabIndex = 2;
            this.lblLength.Text = "Length";
            // 
            // lblSmodeData
            // 
            this.lblSmodeData.AutoSize = true;
            this.lblSmodeData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSmodeData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblSmodeData.Location = new System.Drawing.Point(264, 47);
            this.lblSmodeData.Name = "lblSmodeData";
            this.lblSmodeData.Size = new System.Drawing.Size(48, 14);
            this.lblSmodeData.TabIndex = 16;
            this.lblSmodeData.Text = "Smode";
            // 
            // lblMaxHR
            // 
            this.lblMaxHR.AutoSize = true;
            this.lblMaxHR.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxHR.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblMaxHR.Location = new System.Drawing.Point(191, 76);
            this.lblMaxHR.Name = "lblMaxHR";
            this.lblMaxHR.Size = new System.Drawing.Size(51, 14);
            this.lblMaxHR.TabIndex = 3;
            this.lblMaxHR.Text = "Max HR";
            // 
            // lblMonitorData
            // 
            this.lblMonitorData.AutoSize = true;
            this.lblMonitorData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonitorData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblMonitorData.Location = new System.Drawing.Point(191, 47);
            this.lblMonitorData.Name = "lblMonitorData";
            this.lblMonitorData.Size = new System.Drawing.Size(50, 14);
            this.lblMonitorData.TabIndex = 15;
            this.lblMonitorData.Text = "Monitor";
            // 
            // lblRestHR
            // 
            this.lblRestHR.AutoSize = true;
            this.lblRestHR.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestHR.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblRestHR.Location = new System.Drawing.Point(264, 76);
            this.lblRestHR.Name = "lblRestHR";
            this.lblRestHR.Size = new System.Drawing.Size(55, 14);
            this.lblRestHR.TabIndex = 4;
            this.lblRestHR.Text = "Rest HR";
            // 
            // lblRestHRData
            // 
            this.lblRestHRData.AutoSize = true;
            this.lblRestHRData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestHRData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblRestHRData.Location = new System.Drawing.Point(264, 105);
            this.lblRestHRData.Name = "lblRestHRData";
            this.lblRestHRData.Size = new System.Drawing.Size(55, 14);
            this.lblRestHRData.TabIndex = 14;
            this.lblRestHRData.Text = "Rest HR";
            // 
            // lblMonitor
            // 
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonitor.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblMonitor.Location = new System.Drawing.Point(191, 18);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(50, 14);
            this.lblMonitor.TabIndex = 5;
            this.lblMonitor.Text = "Monitor";
            // 
            // lblMaxHRData
            // 
            this.lblMaxHRData.AutoSize = true;
            this.lblMaxHRData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxHRData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblMaxHRData.Location = new System.Drawing.Point(191, 105);
            this.lblMaxHRData.Name = "lblMaxHRData";
            this.lblMaxHRData.Size = new System.Drawing.Size(51, 14);
            this.lblMaxHRData.TabIndex = 13;
            this.lblMaxHRData.Text = "Max HR";
            // 
            // lblSmode
            // 
            this.lblSmode.AutoSize = true;
            this.lblSmode.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSmode.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblSmode.Location = new System.Drawing.Point(264, 18);
            this.lblSmode.Name = "lblSmode";
            this.lblSmode.Size = new System.Drawing.Size(48, 14);
            this.lblSmode.TabIndex = 6;
            this.lblSmode.Text = "Smode";
            // 
            // lblLengthDAta
            // 
            this.lblLengthDAta.AutoSize = true;
            this.lblLengthDAta.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLengthDAta.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblLengthDAta.Location = new System.Drawing.Point(112, 105);
            this.lblLengthDAta.Name = "lblLengthDAta";
            this.lblLengthDAta.Size = new System.Drawing.Size(47, 14);
            this.lblLengthDAta.TabIndex = 12;
            this.lblLengthDAta.Text = "Length";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartTime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblStartTime.Location = new System.Drawing.Point(391, 18);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(67, 14);
            this.lblStartTime.TabIndex = 8;
            this.lblStartTime.Text = "Start Time";
            // 
            // lblDateData
            // 
            this.lblDateData.AutoSize = true;
            this.lblDateData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblDateData.Location = new System.Drawing.Point(330, 47);
            this.lblDateData.Name = "lblDateData";
            this.lblDateData.Size = new System.Drawing.Size(34, 14);
            this.lblDateData.TabIndex = 11;
            this.lblDateData.Text = "Date";
            // 
            // lblVOzMax
            // 
            this.lblVOzMax.AutoSize = true;
            this.lblVOzMax.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVOzMax.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblVOzMax.Location = new System.Drawing.Point(330, 76);
            this.lblVOzMax.Name = "lblVOzMax";
            this.lblVOzMax.Size = new System.Drawing.Size(55, 14);
            this.lblVOzMax.TabIndex = 9;
            this.lblVOzMax.Text = "VO2Max";
            // 
            // lblVersionData
            // 
            this.lblVersionData.AutoSize = true;
            this.lblVersionData.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersionData.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblVersionData.Location = new System.Drawing.Point(114, 47);
            this.lblVersionData.Name = "lblVersionData";
            this.lblVersionData.Size = new System.Drawing.Size(51, 14);
            this.lblVersionData.TabIndex = 10;
            this.lblVersionData.Text = "Version";
            // 
            // Data_Load
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 491);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "Data_Load";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.DataView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelMinHeartRate;
        private System.Windows.Forms.Label labelMaxHeartRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblWeightData;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblVO2MaxData;
        private System.Windows.Forms.Label lblStartTimeData;
        private System.Windows.Forms.Label lblSmodeData;
        private System.Windows.Forms.Label lblMonitorData;
        private System.Windows.Forms.Label lblRestHRData;
        private System.Windows.Forms.Label lblMaxHRData;
        private System.Windows.Forms.Label lblLengthDAta;
        private System.Windows.Forms.Label lblDateData;
        private System.Windows.Forms.Label lblVersionData;
        private System.Windows.Forms.Label lblVOzMax;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label lblSmode;
        private System.Windows.Forms.Label lblMonitor;
        private System.Windows.Forms.Label lblRestHR;
        private System.Windows.Forms.Label lblMaxHR;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelAverageHeartRate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelSummary;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label labelAverageAltitude;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelMaxAltitude;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelAveragePower;
        private System.Windows.Forms.Label labelMaxPower;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label averagespeed;
        private System.Windows.Forms.Label maxspeed;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label totaldistance;
    }
}

