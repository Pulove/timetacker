﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cycle
{
    public partial class Main : Form
    {
        public static string Path;


        public Main()
        {
            InitializeComponent();
        }

        private void dataViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Data_Load dv = new Data_Load();
            dv.MdiParent = this;
            dv.Show();
        }

        private void graphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Zed_Graph gp = new Zed_Graph();
            gp.MdiParent = this;
            gp.Show();
        }

      

        private void advanceMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdvanceMetrix am = new AdvanceMetrix();
            am.MdiParent=this;
            am.Show();
        }

        private void clearScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.Controls.Clear();

        }

        private void selectedDataViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedData sd = new SelectedData();
            this.Controls.Clear();
            this.Controls.Add(sd);
        }

        private void calenderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CalanderView cv = new CalanderView();
            this.Controls.Clear();
            this.Controls.Add(cv);
        }
    }
}
