﻿namespace Cycle
{
    partial class CalanderView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDateData = new System.Windows.Forms.DataGridView();
            this.monthCalendarData = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDateData)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDateData
            // 
            this.dgvDateData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDateData.Location = new System.Drawing.Point(274, 119);
            this.dgvDateData.Name = "dgvDateData";
            this.dgvDateData.Size = new System.Drawing.Size(348, 162);
            this.dgvDateData.TabIndex = 16;
            // 
            // monthCalendarData
            // 
            this.monthCalendarData.Location = new System.Drawing.Point(15, 119);
            this.monthCalendarData.Name = "monthCalendarData";
            this.monthCalendarData.TabIndex = 15;
            this.monthCalendarData.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarData_DateChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(256, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Data on Date";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(3, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(634, 234);
            this.panel1.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(528, 332);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CalanderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvDateData);
            this.Controls.Add(this.monthCalendarData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "CalanderView";
            this.Size = new System.Drawing.Size(640, 365);
            this.Load += new System.EventHandler(this.CalanderView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDateData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDateData;
        private System.Windows.Forms.MonthCalendar monthCalendarData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
    }
}
