﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using ZedGraph;

namespace Cycle
{
    public partial class Zed_Graph : Form
    {
        delegate void SetTextCallback(string text);
        delegate void axisChangeZedGraphCallBack(ZedGraphControl zg);
        public Thread garthererThread;

        LineItem myCurve, myCurve2, myCurve3, myCurve4, myCurve5,myCurve6;
        PointPairList list1;
        PointPairList list2;
        PointPairList list3;
        PointPairList list4;
        PointPairList list5;
        PointPairList list6;
        GraphPane myPane;


        string[] heartrate;
        string[] speed;
        string[] Cadence;
        string[] Altitude;
        string[] Power;
        string[] Balance;


        public Zed_Graph()
        {
            InitializeComponent();

        }


        public void Thread1()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            heartrate = new string[arrData.Length - index2];
            int j = 0;



            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                heartrate[j] = arrHrdata[0];

                this.SetText1(heartrate[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        private void axisChangeZedGraph(ZedGraphControl zg)
        {
            if (zg.InvokeRequired)
            {
                axisChangeZedGraphCallBack ad = new axisChangeZedGraphCallBack(axisChangeZedGraph);
                zg.Invoke(ad, new object[] { zg });
            }
            else
            {
                zedGraphControl1.AxisChange();
                zg.Invalidate();
                zg.Refresh();
            }
        }
        public void Thread2()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            speed = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                speed[j] = arrHrdata[1];
                this.SetText2(speed[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread3()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            Cadence = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                Cadence[j] = arrHrdata[2];

                this.SetText3(Cadence[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread4()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            Altitude = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                Altitude[j] = arrHrdata[3];
                this.SetText4(Altitude[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread5()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            Power = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                Power[j] = arrHrdata[4];

                this.SetText5(Power[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }

        public void Thread6()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            Balance = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                Balance[j] = arrHrdata[4];

                this.SetText6(Power[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }



        private void TimerCallback(object source, ElapsedEventArgs e)
        {
            garthererThread = new Thread(new ThreadStart(GartherData));
        }

        private void GartherData()
        {
            List<float> gartheredInfo = new List<float>();

            //Do your garthering and parsing here (and put it in the gartheredInfo variable)

            //InformationHolder.Instance().graphData = gartheredInfo;

            zedGraphControl1.Invoke(new MethodInvoker( //you need to have a reference to the form
                    delegate
                    {
                        zedGraphControl1.Invalidate(); //or another method that redraws the graph
                    }));
        }

        private void SetText1(string text)
        {
            if (label1.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText1);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label1.Text = text;
            }
        }

        private void SetText2(string text)
        {
            if (label2.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText2);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label2.Text = text;
            }
        }


        private void SetText3(string text)
        {
            if (label3.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText3);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label3.Text = text;
            }
        }

      

        private void SetText4(string text)
        {
            if (label4.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText4);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label4.Text = text;
            }
        }

        private void SetText5(string text)
        {
            if (label5.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText5);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label5.Text = text;
            }
        }

        private void SetText6(string text)
        {
            if (label6.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText5);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label6.Text = text;
            }
        }
        private void CreateGraph()
        {

            myPane.Title.Text = "My Graph";
            myPane.XAxis.Title.Text = "My X Axis(LINE NUM)";
            myPane.YAxis.Title.Text = "My Y Axis(HR, SPD, CAD, ALT, PWR)";



            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            heartrate = new string[arrData.Length - index2];
            speed = new string[arrData.Length - index2];
            Cadence = new string[arrData.Length - index2];
            Altitude = new string[arrData.Length - index2];
            Power = new string[arrData.Length - index2];
            Balance = new string[arrData.Length - index2];
            int j = 0;



            // Make up some, data arrays based on the Sine function
            double x, y1, y2, y3, y4, y5,y6;
            PointPairList list1 = new PointPairList();
            PointPairList list2 = new PointPairList();
            PointPairList list3 = new PointPairList();
            PointPairList list4 = new PointPairList();
            PointPairList list5 = new PointPairList();
            PointPairList list6 = new PointPairList();
            for (int i = index2 + 1; i < arrData.Length - 1; i += 1)
            {
                string HRData = arrData[i];

                string[] arrHrdata = Regex.Split(HRData, @"\W+");


                heartrate[j] = arrHrdata[0];
                speed[j] = arrHrdata[1];
                Cadence[j] = arrHrdata[2];
                Altitude[j] = arrHrdata[3];
                Power[j] = arrHrdata[4];
                Balance[j] = arrHrdata[5];

                int a = Int32.Parse(heartrate[j]);
                int b = Int32.Parse(speed[j]);
                int c = Int32.Parse(Cadence[j]);
                int d = Int32.Parse(Altitude[j]);
                int e = Int32.Parse(Power[j]);
                int f = Int32.Parse(Balance[j]);

                x = i;
                y1 = a;
                y2 = b;
                y3 = c;
                y4 = d;
                y5 = e;
                y6 = e;

                LineItem curve = zedGraphControl1.GraphPane.CurveList["HR"] as LineItem;
                // Get the PointPairList
                IPointListEdit list = curve.Points as IPointListEdit;
                list.Add(x, y1);
                LineItem curve2 = zedGraphControl1.GraphPane.CurveList["Speed"] as LineItem;
                // Get the PointPairList
                IPointListEdit list12 = curve2.Points as IPointListEdit;
                list12.Add(x, y2);
                LineItem curve3 = zedGraphControl1.GraphPane.CurveList["CAD"] as LineItem;
                // Get the PointPairList
                IPointListEdit list13 = curve3.Points as IPointListEdit;
                list13.Add(x, y3);
                LineItem curve4 = zedGraphControl1.GraphPane.CurveList["ALT"] as LineItem;
                // Get the PointPairList
                IPointListEdit list14 = curve4.Points as IPointListEdit;
                list14.Add(x, y4);
                LineItem curve5 = zedGraphControl1.GraphPane.CurveList["PWR"] as LineItem;
                // Get the PointPairList
                IPointListEdit list15 = curve5.Points as IPointListEdit;
                list15.Add(x, y5);
                LineItem curve6 = zedGraphControl1.GraphPane.CurveList["BAL"] as LineItem;
                // Get the PointPairList
                IPointListEdit list16 = curve6.Points as IPointListEdit;
                list15.Add(x, y6);

                axisChangeZedGraph(zedGraphControl1);
                Thread.Sleep(700);
            }
        }

        private void Graph_Load(object sender, EventArgs e)
        {
            myPane = zedGraphControl1.GraphPane;
            list1 = new PointPairList();
            list2 = new PointPairList();
            list3 = new PointPairList();
            list4 = new PointPairList();
            list5 = new PointPairList();
            list6 = new PointPairList();
            myCurve = myPane.AddCurve("HR",
                 list1, Color.Red, SymbolType.Diamond);


            myCurve2 = myPane.AddCurve("Speed",
                 list2, Color.Blue, SymbolType.Circle);


            myCurve3 = myPane.AddCurve("CAD",
               list3, Color.Green, SymbolType.Diamond);


            myCurve4 = myPane.AddCurve("ALT",
                 list4, Color.Purple, SymbolType.Diamond);


            myCurve5 = myPane.AddCurve("PWR",
                 list5, Color.Brown, SymbolType.Diamond);

            myCurve6 = myPane.AddCurve("BAL",
                 list6, Color.DarkGray, SymbolType.Diamond);
            Thread tid1 = new Thread(new ThreadStart(Thread1));
            Thread tid2 = new Thread(new ThreadStart(Thread2));
            Thread tid3 = new Thread(new ThreadStart(Thread3));
            Thread tid4 = new Thread(new ThreadStart(Thread4));
            Thread tid5 = new Thread(new ThreadStart(Thread5));
            Thread tid6 = new Thread(new ThreadStart(Thread6));
            Thread tidgc = new Thread(new ThreadStart(CreateGraph));
            tid1.Start();
            tid2.Start();
            tid3.Start();
            tid4.Start();
            tid5.Start();
            tidgc.Start();
        }

     
        private void chkHR_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHR.Checked == true)
            {
                myCurve.IsVisible = false;
                chkHR.Invalidate();
            }
            else
            {
                myCurve.IsVisible = true;
                chkHR.Invalidate();
            }
        }

        private void chkSpeed_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSpeed.Checked == true)
            {
                myCurve2.IsVisible = false;
                chkSpeed.Invalidate();
            }
            else
            {
                myCurve2.IsVisible = true;
                chkSpeed.Invalidate();
            }
        }

        private void chkCad_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCad.Checked == true)
            {
                myCurve3.IsVisible = false;
                chkCad.Invalidate();
            }
            else
            {
                myCurve3.IsVisible = true;
                chkCad.Invalidate();
            }
        }

        private void chkAlt_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlt.Checked == true)
            {
                myCurve4.IsVisible = false;
                chkAlt.Invalidate();
            }
            else
            {
                myCurve4.IsVisible = true;
                chkAlt.Invalidate();
            }
        }

        private void chkPower_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPower.Checked == true)
            {
                myCurve5.IsVisible = false;
                chkPower.Invalidate();
            }
            else
            {
                myCurve5.IsVisible = true;
                chkPower.Invalidate();
            }
        }
   }
}
