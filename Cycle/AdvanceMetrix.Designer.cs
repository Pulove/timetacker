﻿namespace Cycle
{
    partial class AdvanceMetrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvanceMetrix));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTSS = new System.Windows.Forms.Label();
            this.LBL = new System.Windows.Forms.Label();
            this.lblIF = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblFTP = new System.Windows.Forms.Label();
            this.FTP = new System.Windows.Forms.Label();
            this.lblAvgPower = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBoxClose = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBoxClose);
            this.panel1.Controls.Add(this.lblTSS);
            this.panel1.Controls.Add(this.LBL);
            this.panel1.Controls.Add(this.lblIF);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblFTP);
            this.panel1.Controls.Add(this.FTP);
            this.panel1.Controls.Add(this.lblAvgPower);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 231);
            this.panel1.TabIndex = 0;
            // 
            // lblTSS
            // 
            this.lblTSS.AutoSize = true;
            this.lblTSS.BackColor = System.Drawing.Color.Transparent;
            this.lblTSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTSS.Location = new System.Drawing.Point(250, 150);
            this.lblTSS.Name = "lblTSS";
            this.lblTSS.Size = new System.Drawing.Size(14, 20);
            this.lblTSS.TabIndex = 21;
            this.lblTSS.Text = ".";
            // 
            // LBL
            // 
            this.LBL.AutoSize = true;
            this.LBL.BackColor = System.Drawing.Color.Transparent;
            this.LBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL.Location = new System.Drawing.Point(35, 150);
            this.LBL.Name = "LBL";
            this.LBL.Size = new System.Drawing.Size(161, 20);
            this.LBL.TabIndex = 20;
            this.LBL.Text = "Training Stress Score";
            // 
            // lblIF
            // 
            this.lblIF.AutoSize = true;
            this.lblIF.BackColor = System.Drawing.Color.Transparent;
            this.lblIF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIF.Location = new System.Drawing.Point(250, 100);
            this.lblIF.Name = "lblIF";
            this.lblIF.Size = new System.Drawing.Size(14, 20);
            this.lblIF.TabIndex = 19;
            this.lblIF.Text = ".";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Intensity Factor";
            // 
            // lblFTP
            // 
            this.lblFTP.AutoSize = true;
            this.lblFTP.BackColor = System.Drawing.Color.Transparent;
            this.lblFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFTP.Location = new System.Drawing.Point(250, 59);
            this.lblFTP.Name = "lblFTP";
            this.lblFTP.Size = new System.Drawing.Size(14, 20);
            this.lblFTP.TabIndex = 15;
            this.lblFTP.Text = ".";
            // 
            // FTP
            // 
            this.FTP.AutoSize = true;
            this.FTP.BackColor = System.Drawing.Color.Transparent;
            this.FTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FTP.Location = new System.Drawing.Point(35, 59);
            this.FTP.Name = "FTP";
            this.FTP.Size = new System.Drawing.Size(136, 20);
            this.FTP.TabIndex = 14;
            this.FTP.Text = "Normalized Power";
            // 
            // lblAvgPower
            // 
            this.lblAvgPower.AutoSize = true;
            this.lblAvgPower.BackColor = System.Drawing.Color.Transparent;
            this.lblAvgPower.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgPower.Location = new System.Drawing.Point(250, 19);
            this.lblAvgPower.Name = "lblAvgPower";
            this.lblAvgPower.Size = new System.Drawing.Size(14, 20);
            this.lblAvgPower.TabIndex = 13;
            this.lblAvgPower.Text = ".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Power Balance";
            // 
            // pictureBoxClose
            // 
            this.pictureBoxClose.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxClose.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxClose.Image")));
            this.pictureBoxClose.Location = new System.Drawing.Point(473, -62);
            this.pictureBoxClose.Name = "pictureBoxClose";
            this.pictureBoxClose.Size = new System.Drawing.Size(40, 38);
            this.pictureBoxClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxClose.TabIndex = 22;
            this.pictureBoxClose.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AdvanceMetrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(351, 231);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Name = "AdvanceMetrix";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdvanceMetrix";
            this.Load += new System.EventHandler(this.AdvanceMetrix_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxClose;
        private System.Windows.Forms.Label lblTSS;
        private System.Windows.Forms.Label LBL;
        private System.Windows.Forms.Label lblIF;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFTP;
        private System.Windows.Forms.Label FTP;
        private System.Windows.Forms.Label lblAvgPower;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}